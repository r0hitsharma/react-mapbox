import React, { Component } from 'react'
import ReactMapboxGl, { Layer, Feature } from "react-mapbox-gl"

const Map = ReactMapboxGl({
  accessToken: process.env.REACT_APP_MAPBOX_TOKEN
})

class MapComponent extends Component {
	render () {
		return <Map
		  style="mapbox://styles/mapbox/streets-v8"
		  containerStyle={{
		    height: "100%",
		    width: "100%"
		  }}>
		    <Layer
		      type="symbol"
		      id="marker"
		      layout={{ "icon-image": "marker-15" }}>
					<Feature
						coordinates={[-0.481747846041145, 51.3233379650232]}
						onClick={() => { this.props.navigateTo(1) }}
					/>
		    </Layer>
		</Map>
	}
}

export default MapComponent