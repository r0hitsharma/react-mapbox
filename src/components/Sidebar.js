import React, { Component } from 'react'
import { Link } from "react-router-dom"

class Sidebar extends Component {
	constructor(props) {
		super(props)

		this.state = {
			selectedDevice: null
		}
	}

	static getDerivedStateFromProps(nextProps, prevState) {
		console.log(nextProps)
		return { selectedDevice: nextProps.match.params.id || 42 }
	}

	render() {
		// console.log(this.props)
		// const { match } = this.props
		// console.log(match.params.id)

		return <div id="sidebar">
			Sidebar<br/>
			<h2>{ this.state.selectedDevice }</h2>
			<Link to="/sidebar/1">1</Link><br/>
			<Link to="/sidebar/2">2</Link>
		</div>
	}
}

export default Sidebar