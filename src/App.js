import React, { Component } from 'react'
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom"
import Map from './components/Map'
import Sidebar from './components/Sidebar'
import './App.css'

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <NavLink to="/map">Map</NavLink>
          <NavLink to="/sidebar">Sidebar</NavLink>

          <Route
            path="/"
            component={ Parent }
          />
        </div>
      </Router>
    )
  }
}

class Parent extends Component {
  render() {
    // console.log(this.props)
    const { location, history } = this.props

    let cl = null
    if(location.pathname.includes("sidebar"))
      cl = "sidebar"

    return (
      <div id="app" className={ cl }>
        <Map
          navigateTo={ id => history.push(`/sidebar/${ id }`) }
        />
        <Route
          path="/"
          component={ Sidebar }
        />
          {/*
          render={(props) => <Sidebar some={6} { ...props } /> }
          exact path={["/sidebar", "/sidebar/:id"]}
          */}
      </div>
    )
  }
}

export default App
